export const light = () => ({
  colors: {
    primary: "#0070f3",
  },
});

export const dark = () => ({
  fonts: {
    family: "Open Sans",
    size: "14px",

    sideBar: {
      size: "16px",
    },
  },
  colors: {
    main: {
      bgPrimary: "#32363d",
      bgSecundary: "#ececec",
      textPrimary: "#b6b6b6",
      textSecundary: "#141418",
    },
    sideBar: {
      bgPrimary: "#23232e",
      bgSecundary: "#141418",
      textPrimary: "#b6b6b6",
      textSecundary: "#ececec",
    },
    primary: "#0070f3",
  },
});
