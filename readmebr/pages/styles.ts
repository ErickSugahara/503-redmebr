import styled from "styled-components";

export const Container = styled.body`
  top: 0;
  left: 0;
  padding: 0;
  margin: 0;
  font-size: ${(props) => props.theme.fonts.size};
  font-family: ${(props) => props.theme.fonts.family};

  background: ${(props) => props.theme.colors.main.bgPrimary};
  color: ${(props) => props.theme.colors.main.textPrimary};
  margin: 0;

  main {
    padding: 1rem;

    .container-header {
      padding: 1rem;
      width: 100%;
      display: flex;
      flex-direction: column;
      align-items: center;

      .container-links {
        width: 100%;
        display: flex;
        justify-content: end;
        flex-direction: row-reverse;
        height: 10%;

        > ul {
          margin: 8px;
          display: flex;
          justify-content: end;
          list-style-type: none;

          > li {
            margin-right: 24px;
            padding: 0;

            font-size: 24px;
            cursor: pointer;

            transition: ease 0.5s color;

            :hover {
              color: orange;
            }
          }
        }
      }

      h1 {
        margin: 0;
        font-family: ${(props) => props.theme.fonts.family};
      }
    }
  }

  @media only screen and (min-width: 1024px) {
    main {
      .content {
        width: 100%;
        display: grid;
        grid-template-columns: 1fr 1fr 1fr 1fr;
        grid-template-rows: 15vh 15vh 15vh 15vh;
        grid-template-areas:
          "l l a b"
          "l l c d"
          "e f m m";
        grid-gap: 15px;
      }
    }

    footer {
    }
  }

  @media only screen and (max-width: 1024px) {
    main {
      .content {
        width: 100%;
        display: grid;
        grid-template-columns: 1fr 1fr 1fr;
        grid-template-rows: 15vh 15vh 15vh 15vh;
        grid-template-areas:
          "l l a"
          "l l b"
          "c m m"
          "d e f";
        grid-gap: 15px;
      }
    }

    footer {
    }
  }

  @media only screen and (max-width: 600px) {
    main {
      .content {
        width: 100%;
        display: flex;
        flex-direction: column;
      }
    }
    footer {
    }
  }
`;
