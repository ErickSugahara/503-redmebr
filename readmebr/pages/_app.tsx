import App from "next/app";
import { ThemeProvider } from "styled-components";
import GlobalStyle from "./globalStyle";
import { dark } from "../themes/default";

const theme = dark;

export default class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <Component {...pageProps} />
      </ThemeProvider>
    );
  }
}
