import Head from "next/head";
import SideBar from "../components/SideBar";
import Card from "../components/Card";

import { Container } from "./styles";

export default function Home() {
  return (
    <Container>
      <Head>
        <title>ReadME_BR</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className="container-header">
          <header className="container-links">
            <ul>
              <li>Principal</li>
              <li>Fazer um post</li>
              <li>Buscar</li>
            </ul>
          </header>
          <h1>Bem Vindo ao ReadME_BR</h1>
          <sub>Sua base de conheciment em pt-br</sub>
        </div>
        <div className="content">
          <Card grid="l" />
          <Card grid="m" />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <div />
        </div>
      </main>
      <footer></footer>
    </Container>
  );
}
