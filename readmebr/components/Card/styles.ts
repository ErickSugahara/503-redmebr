import styled from "styled-components";

export const Container = styled.div`
  grid-area: ${(props) => (props.grid ? props.grid : null)};
  background: ${(props) => props.theme.colors.main.bgSecundary};
  background-image: ${(props) => `url(${props.bgImage})`};
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
  border-radius: 5px;
  cursor: pointer;

  transition: transform 0.2s;

  :hover {
    transform: scale(1.02);
  }
`;
