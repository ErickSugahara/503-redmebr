import React from "react";

import { Container } from "./styles";

export interface ICard {
  grid?: string;
}

const Card: React.FC<ICard> = (props) => {
  const { grid } = props;

  return (
    <Container
      grid={grid}
      bgImage="https://blog.rocketseat.com.br/content/images/2019/07/Prototipac-a-o-1.png"
    ></Container>
  );
};

export default Card;
